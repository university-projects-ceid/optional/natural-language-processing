const exams = [
	{
		name: 'YLE (STARTERS)',
		price: 60.0,
	},
	{
		name: 'YLE (MOVERS)',
		price: 60.0,
	},
	{
		name: 'YLE (FLYERS)',
		price: 60.0,
	},
	{
		name: 'KET FOR SCHOOLS',
		price: 80.0,
	},
	{
		name: 'PET FOR SCHOOLS',
		price: 85.0,
	},
	{
		name: 'FCE FOR SCHOOLS',
		price: 165.0,
	},
	{
		name: 'CAE',
		price: 155.0,
	},
	{
		name: 'CPE',
		price: 185.0,
	},
];

module.exports = exams;
