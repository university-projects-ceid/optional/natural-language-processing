/**
 * Main Server Module.
 */
// JS Libs.
const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const yaml = require('yamljs');
const swaggerUi = require('swagger-ui-express');

// Project files.
const config = require('./config');
const ApiRouter = require('./api');

const logger = config.getLogger(__filename);
const app = express();

const swaggerDocument = yaml.load('./src/swaggerConfig.yaml');

// Connect to database
mongoose.connect(
	config.getDatabaseUrl(),
	{
		useNewUrlParser: true,
	},
	err => {
		if (err) logger.error(err);
	}
);
mongoose.Promise = global.Promise;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use('/api/v1', ApiRouter);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// If request doesn't get handled by the routers above
// throw error
app.use((error, req, res, next) => {
	if (error) {
		next(error);
	} else {
		const error = new Error('Not found');
		error.status = 404;
		next(error);
	}
});

app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({
		error: {
			message: error.message,
		},
	});
});

// Port
const port = config.backendPort;
app.listen(port, () => logger.error(`Listening on port ${port}...`));
