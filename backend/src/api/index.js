/**
 * Main api router.
 */

const express = require('express');
const router = express.Router();

const config = require('~root/src/config');

const fileUploadRouter = require('~routes/file-upload');
const articleRouter = require('~routes/articles');
const lemmaRouter = require('~routes/lemmas');
const searchRouter = require('~routes/search');
const characteristicRouter = require('~routes/characteristics');

router.use('/lemmas', lemmaRouter);
router.use('/articles', articleRouter);
router.use('/search', searchRouter);
router.use('/file-upload', fileUploadRouter);
router.use('/characteristics', characteristicRouter);


module.exports = router;
