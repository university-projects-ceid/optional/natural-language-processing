/*********************
  Lemmas controller
 *********************/

// Js libs
const mongoose = require('mongoose');

// Libs authored by team
const logger = require('~root/src/config').getLogger(__filename);
const config = require('~root/src/config');

// Services
const lemmaService = require('~services/lemma');
const articleService = require('~services/article');



/******************************
   Exported module functions
******************************/

// GET lemmas for each individual article
module.exports.get_distinct_lemmas = async (req, res, next) => {
	const closedClass = ["CD", "CC", "DT", "EX", "IN", "LS", "MD", "PDT", "POS", "PRP", "PRP$", "RP", "TO ", "UH ", "WDT", "WP", "WP$", "WRB"];
	try {
		const lemmas = [];
		let article_ids = [];
		let article_lemmas = {};

		const deletePrevious = await lemmaService.deleteByQuery();
		const articles = await articleService.getByQuery();

		for (let index = 0; index < articles.length; index++) {
			const article = articles[index];
			const result = article.postags;		
			const sanitized = result.filter(obj => {
				if (obj['pos'] 
					&& obj['lemma'] 
					&& !closedClass.includes(obj['pos']) 
					&& obj['tag']!="punctuation"){
					const lemma = obj['lemma'];
					return lemma;
				}
			});

			// push the lemmas into an array
			let lemma_array = []
			sanitized.forEach(token => {
				if (token.lemma.length > 1){
					// Remove fullstops and commas (e.g emails)
					const sanitizedLemma = token.lemma.replace(/[.,\s]/g, '');
					lemmas.push(sanitizedLemma);
					lemma_array.push(sanitizedLemma);
				}
			});

			// find the term frequencies for each lemma
			let lemma_tfs = {}
			lemma_array.forEach( lemma => {
				let count = lemma_array.count(lemma);
				if (!lemma_tfs[lemma]){
					lemma_tfs[lemma]=count/(lemma_array.length);
				}
			});

			// assign the term frequencies to each article
			article_lemmas[article._id]=lemma_tfs;
			article_ids.push(article._id);

		}

		// Discretize all the lemmas
		let results = [...new Set(lemmas)];

		// let data = {}
		// results.forEach(lemma => {	
		// 	data[lemma] = {};
		// 	data[lemma]['appears_in'] = 0.0;
		// 	article_ids.forEach( _id => {
		// 		const tf = article_lemmas[_id][lemma];
		// 		if (tf) {
		// 			data[lemma][_id] = tf;
		// 			data[lemma]['appears_in'] = data[lemma]['appears_in'] + 1;
		// 		}else {
		// 			data[lemma][_id] = 0.0;
		// 		}
		// 	});
		// 	article_ids.forEach( _id => {
		// 		const idf = Math.log10(article_ids.length/data[lemma]['appears_in']);
		// 		data[lemma][_id] = data[lemma][_id]*idf;
		// 	});
		// });

		let data = []
		for (let i = 0; i < results.length; i++) {
			const lemma = results[i];
			let appearences = 0.0;
			let articles = []
			article_ids.forEach( _id => {
				let termFrequency = article_lemmas[_id][lemma];

				if (termFrequency) {
					appearences = appearences + 1;
				}else {
					termFrequency = 0.0;
				}

				let obj = {
					articleId: _id,
					weight: termFrequency 
				}
				articles.push(obj);
			});

			article_ids.forEach( (_id, index) => {
				const idf = Math.log10(article_ids.length/appearences);
				// articles[index].invDocFreq = idf;
				articles[index].weight = articles[index].weight*idf;

			});

			const insertion = await lemmaService.createLemma({
				_id: mongoose.Types.ObjectId(),
				name: lemma,
				appearences: appearences,
				articles: articles
			});

			data.push(insertion);

		}

		return res.status(200).json(data);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(__filename);
		next(error);
	}
};

// DELETE all distinct lemmas
module.exports.delete_distinct_lemmas = async (req, res, next) => {
	try {

		await lemmaService.deleteByQuery();

		res.sendStatus(200);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(error);
		next(error);
	}
};

// GET all lemmas & weights from db
module.exports.get_all_lemmas = async (req, res, next) => {
	try {

		let lemmas = await lemmaService.getByQuery();

		return res.status(200).json(lemmas);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(error);
		next(error);
	}
};

/******************************
       Helper functions
******************************/

// Count number of times item appears in Array prototype
Object.defineProperties(Array.prototype, {
    count: {
        value: function(query) {
            /* 
               Counts number of occurrences of query in array, an integer >= 0 
               Uses the javascript == notion of equality.
            */
            var count = 0;
            for(let i=0; i<this.length; i++)
                if (this[i]==query)
                    count++;
            return count;
        }
    }
});
