const router = require('express').Router();
const controller = require('./controller');
const config = require('~root/src/config');


router.get('/distinct', controller.get_distinct_lemmas);

router.delete('/distinct', controller.delete_distinct_lemmas);

router.get('/all', controller.get_all_lemmas);

module.exports = router;
