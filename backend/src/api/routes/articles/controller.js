/*********************
  Articles controller
 *********************/

// Js libs
const mongoose = require('mongoose');
const cheerio = require('cheerio');
const request = require('request');
const posTagger = require( 'wink-pos-tagger' );


// Libs authored by team
const logger = require('~root/src/config').getLogger(__filename);
const config = require('~root/src/config');

// Services
const articleService = require('~services/article');



/******************************
   Exported module functions
******************************/


// GET all articles from db
module.exports.get_all_articles = async (req, res, next) => {
	try {
		const articles = await articleService.getByQuery();

		return res.status(200).json(articles);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(__filename);
		next(error);
	}
};

// GET article by id from db
module.exports.get_article_by_id = async (req, res, next) => {
	console.log(req.params.articleId)
	try {
		const article = await articleService.getById(req.params.articleId);

		return res.status(200).json(article);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(__filename);
		next(error);
	}
};

// DELETE article by id from db
module.exports.delete_article_by_id = async (req, res, next) => {
	try {
		const id = req.params.articleId;
		await articleService.deleteArticle(id);

		res.sendStatus(200);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(error);
		next(error);
	}
};

// POST top new york times articles to db
module.exports.scrape_nytimes_articles = async (req, res, next) => {
	try {
		// Invoke the posTagger
		const tagger = posTagger();

		// Get all the links for top articles
		let links = await get_ny_times_links();
		let results = [];

		// For all links scrape their data
		for (let i = 0; i < links.length; i++) {
			const link = links[i];
			const article = await get_ny_times_article(link);

			// Only store to db if there is no empty key 
			if(	article.headline!=""
				&article.summary!=""
				&article.body!=""){
					const postags = await tagger.tagSentence(article.body);
					const result = await articleService.createArticle({
						_id: mongoose.Types.ObjectId(),
						url:link,
						headline: article.headline,
						summary: article.summary,
						body: article.body,
						postags: postags
					});
					results.push(result);
				}

		}

		res.status(201).json(results);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(error);
		next(error);
	}
};




/******************************
Helper functions (not exported)
******************************/


// GET nytimes.com and scrape top article links
function get_ny_times_links() {
	return new Promise((resolve, reject) => {

		const url = 'https://www.nytimes.com';
		let results = [];

		request(url, (error, response, html)=> {
			const $ = cheerio.load(html);
			if(!error && response.statusCode==200){
				const links = $('.assetWrapper');
				for (let index = 0; index < links.length; index++) {
					const link = links[index];
					const anchor = $(link).find("a").attr("href");
					results.push(url + anchor);
				}
				resolve(results);
			}
		});

	});
}

// GET nytimes article and scrape for article data
function get_ny_times_article(url) {
	return new Promise((resolve, reject) => {
		// GET the article html
		request(url, (error, response, html)=> {
			if(!error && response.statusCode==200){
				const $ = cheerio.load(html);
				// If status=200 get headline, summary and body
				const headline = $('h1').attr('itemprop','headline').find('span').text();
				const summary = $('#article-summary').text();
				const body = $('.StoryBodyCompanionColumn').find('p').text();
				
				// console.log(headline);
				// console.log(summary);
				// console.log(body);

				obj = {
					headline: headline,
					summary: summary,
					body: body
				}

				resolve(obj)
			}else {
				resolve({
					headline: "",
					summary: "",
					body: ""
				})
			}
		});

	});
}