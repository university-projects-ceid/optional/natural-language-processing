const router = require('express').Router();
const controller = require('./controller');
const config = require('~root/src/config');


router.get('/all', controller.get_all_articles);

router.get('/:articleId', controller.get_article_by_id);

router.post('/scrape/nytimes', controller.scrape_nytimes_articles);

router.delete('/:articleId', controller.delete_article_by_id);


module.exports = router;
