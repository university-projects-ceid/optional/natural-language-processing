/****************************
  Characteristics controller
*****************************/

// Js libs
const fs = require('fs');
const mongoose = require('mongoose');
const natural = require('natural');
const path = require('path');
const jaccard = require('jaccard');
const cosine = require ('cos-similarity');

// Libs authored by team
const logger = require('~root/src/config').getLogger(__filename);
const config = require('~root/src/config');

// Services
const characteristicService = require('~root/src/api/services/characteristic');

// Declarations
const trainPath = config.uploadTrain;
const comparePath = config.uploadCompare;



/******************************
   Exported module functions
******************************/

// Extract features from stored documents
module.exports.create_characteristics = async (req, res, next) => {
	try {
        let response = [];
        let collection = [];
        let lengths = [];
        let documentAppearences = {}
        let countTotalAppearences = {};
        let nullVector = {}; 
        let fileFeatures = [];
        
        // Get files in dir and delete old entries in db
        const files = await get_files_in_dir(trainPath);
        await characteristicService.deleteByQuery();

        // Open all documents, tokenize and get stems
        for (let i = 0; i < files.length; i++) {
            const file = files[i];
            fileFeatures.push(get_file_features(file));
            const filePath = path.join(trainPath, file);
            const contents = await read_file(filePath);
            let stems = tokenize_and_stem(contents);
            let countIndividualAppearences = {};
            lengths.push(stems.length);

            // Find tfs for each document & count appearences of stems in documents
            for (let index = 0; index < stems.length; index++) {
                nullVector[stems[index]] = 0;
                // Document appearences for idf metric
                if(documentAppearences.hasOwnProperty(stems[index])){
                    documentAppearences[stems[index]].push(i);
                    documentAppearences[stems[index]] = [...new Set(documentAppearences[stems[index]])]
                }else{
                    documentAppearences[stems[index]] = [i];
                }
                // Individual appearences for each doc
                if(countIndividualAppearences.hasOwnProperty(stems[index])) {
                    countIndividualAppearences[stems[index]]++;
                }else{
                    countIndividualAppearences[stems[index]] = 0;
                }
            }
            collection.push(countIndividualAppearences);
        }

        // Calculate tfidf vectors itteratively for each document and store to db
        for (let i = 0; i < collection.length; i++) {
            let vector = {...nullVector};
            for (const key in collection[i]) {
                if (collection[i].hasOwnProperty(key)) {
                    if(typeof(documentAppearences[key])=="object"){
                        documentAppearences[key] = Math.log10(collection.length/documentAppearences[key].length);
                    }
                    vector[key] = (collection[i][key]/lengths[i])*documentAppearences[key];
                }
            }
            const result = await characteristicService.createCharacteristic({
                _id: mongoose.Types.ObjectId(),
                features: fileFeatures[i].features,
                vector:vector,
                tfIdfVector: Object.values(vector)
            });
            response.push(result);
        }

        // Store idf for comparison vectors
        const storeIdf = await characteristicService.createCharacteristic({
            _id: mongoose.Types.ObjectId(),
            features: [],
            vector: documentAppearences,
            idf: true
        });
        response.push(storeIdf);

        // Store null vector to add tfidf values from comparison
        const storeNull = await characteristicService.createCharacteristic({
            _id: mongoose.Types.ObjectId(),
            features: [],
            vector:{...nullVector},
            null: true
        });
        response.push(storeNull);

		res.status(201).json(response);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(error);
		next(error);
	}
};

// Function that compares tfidf vector of input documents with items in db 
module.exports.compare_characteristics = async (req, res, next) => {
	try {
        let response;
        let collection = [];
        let documents = [];
        let lengths = [];
        const stemIDF = await characteristicService.getIdfVector();
        const tfidfVector = await characteristicService.getNullVector();
        
        // Get files in dir and delete old entries in db
        const files = await get_files_in_dir(comparePath);

        // Open all documents, tokenize and get stems
        for (let i = 0; i < files.length; i++) {
            const file = files[i];
            const filePath = path.join(comparePath, file);
            const contents = await read_file(filePath);
            let stems = tokenize_and_stem(contents);
            let stemTF = {...tfidfVector};
            lengths.push(stems.length);
            
            // Find tfs for each document & count appearences of stems in documents
            for (let index = 0; index < stems.length; index++) {
                if (tfidfVector[stems[index]] >= 0){
                    stemTF[stems[index]] = 1 + (stemTF[stems[index]]);
                }
            }
            collection.push(stemTF);
        }
        
        // Calculate tfidf vectors itteratively for each document and store to db
        for (let i = 0; i < collection.length; i++) {
            let vector = {...tfidfVector};
            for (const key in collection[i]) {
                if (collection[i].hasOwnProperty(key)) {
                    vector[key] = (collection[i][key]/lengths[i])*stemIDF[key];
                }
            }

            documents.push({
                file: files[i],
                vector:vector,
                vals: Object.values(vector)
            });
        }

        const storedDocuments = await characteristicService.getOnlyVectors();
        const jaccardSimilarity = calculate_jaccard_similarity(storedDocuments, documents);
        const cosineSimilarity = calculate_cosine_similarity(storedDocuments, documents);

        response = {
            cosineSimilarity: cosineSimilarity,
            jaccardSimilarity: jaccardSimilarity
        }

		res.status(201).json(response);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(error);
		next(error);
	}
};


/***********************************
   Helper functions (not exported)
************************************/

// Tokenize and stem a text body
tokenize_and_stem = (text) => {
    // invoke stemmer from natural library
    natural.PorterStemmer.attach();
    let result = text.tokenizeAndStem();
    return result;
};

// Get file features from the file name
get_file_features = (fileName) => {
    let segments = fileName.split(".");
    segments.pop();
    let result = {features: segments};
    return result;
};

// Get all files in given path
get_files_in_dir = (givenPath) => {
    return new Promise((resolve, reject) => {
        fs.readdir(givenPath, (error, files) => {
            resolve(files);
        });
    });
};

// Return contents of read file
read_file = (filePath) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, {encoding: 'utf-8'}, (error, contents) => {
            let sanitizedContent = contents.replace(/([^.@\s]+)(\.[^.@\s]+)*@([^.@\s]+\.)+([^.@\s]+)/g, "");
            sanitizedContent = contents.replace(/[0-9]/g, "");
            resolve(sanitizedContent);
        });
    });
}

// Calculate jaccard similarity
calculate_jaccard_similarity = (storedDocs, comparedDocs) => {
    let response = [];
    for (let i = 0; i < comparedDocs.length; i++) {
        const document = comparedDocs[i];
        let jaccardSimilarity = {
            file: document.file,
            features: [],
            jaccardIndex: 0
        }
        for (let j = 0; j < storedDocs.length; j++) {
            const storedDocument = storedDocs[j];
            let vector1 = [...storedDocument.tfIdfVector];
            let vector2 = [...document.vals];
            let jaccardIndex = jaccard.index(vector1, vector2);
            if (jaccardIndex > jaccardSimilarity.jaccardIndex){
                jaccardSimilarity.features = storedDocument.features;
                jaccardSimilarity.jaccardIndex = jaccardIndex;
            }
        }
        response.push(jaccardSimilarity);
    }
    return response;
}

// Calculate cosine similarity
calculate_cosine_similarity = (storedDocs, comparedDocs) => {
    let response = [];
    for (let i = 0; i < comparedDocs.length; i++) {
        const document = comparedDocs[i];
        let cosineSimilarity = {
            file: document.file,
            features: [],
            cosineIndex: 0
        }
        for (let j = 0; j < storedDocs.length; j++) {
            const storedDocument = storedDocs[j];
            let vector1 = [...storedDocument.tfIdfVector];
            let vector2 = [...document.vals];
            let cosineIndex = cosine(vector1, vector2);
            if (cosineIndex > cosineSimilarity.cosineIndex){
                cosineSimilarity.features = storedDocument.features;
                cosineSimilarity.cosineIndex = cosineIndex;
            }
        }
        response.push(cosineSimilarity);
    }
    return response;
}