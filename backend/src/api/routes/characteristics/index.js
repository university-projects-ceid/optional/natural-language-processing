const router = require('express').Router();
const controller = require('./controller');
const config = require('~root/src/config');


router.get('/train', controller.create_characteristics);

router.get('/compare', controller.compare_characteristics);

module.exports = router;
