/*********************
  Lemmas controller
 *********************/

// Js libs
const mongoose = require('mongoose');

// Libs authored by team
const logger = require('~root/src/config').getLogger(__filename);
const config = require('~root/src/config');

// Services
const lemmaService = require('~services/lemma');
const searchService = require('~services/search');
const articleService = require('~services/article');



/******************************
   Exported module functions
******************************/

// GET all articles from db
module.exports.search_for_words = async (req, res, next) => {
	try {

		// Get the search phrase from slug
		const phrase = searchService.getSearchPhrase(req.body.phrase);
		
		// Split the phrase to words
		const tokens = phrase.split(" ");
		// Get the possible phrase vocalizations
		// const vocalizations = vocalize(phrase);

		// Create the regex for the search
		const searchPhrases = searchService.prepareRegex(tokens);

		// The query to be run
		const query = { name: { $in: searchPhrases } };

		const results = await searchService.getSearchResults(query);

		let sum = results[0].articles;
		
		if (tokens.length>1){
			for (let i = 1; i < results.length; i++) {
				const articles = results[i].articles;
				articles.forEach((article,index) => {
					sum[index].weight += article.weight;
				});
			}
			
		}

		let response = [];

		sum = sum.sort(compare);
		let ids = []; 
		for(let object of sum){
			if (object.weight!=0){
				response.push(await articleService.getById(object.articleId))
			}else{
				break;
			}
		}

		// let response = await articleService.getByQuery({_id : {$all:ids}});

		res.status(200).json(response);
		// res.status(200).json({
			// professionals: results.professionals,
			// categories: results.categories,
		// });
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(__filename);
		next(error);
	}
};



/******************************
   Exported module functions
******************************/

// Function that compares two numbers
function compare(a, b) {

	// Use toUpperCase() to ignore character casing
	const itemA = a.weight;
	const itemB = b.weight;

	let comparison = 0;
	if (itemA > itemB) {
		comparison = 1;
	} else if (itemA < itemB) {
		comparison = -1;
	}
	return comparison*-1;

}