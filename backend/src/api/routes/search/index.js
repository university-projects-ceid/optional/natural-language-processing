const router = require('express').Router();
const controller = require('./controller');

router.post('/', controller.search_for_words);

module.exports = router;
