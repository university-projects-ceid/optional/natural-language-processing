const multer = require('multer');

const router = require('express').Router();
const controller = require('./controller');
const config = require('~root/src/config');

// Set the location and name settings for files uploaded
const storageTrain = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadTrain);
	},
	filename: (req, file, cb) => {
		cb(null, file.originalname);
	},
});

const storageCompare = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadCompare);
	},
	filename: (req, file, cb) => {
		cb(null, file.originalname);
	},
});

// Only accept text files
const fileFilter = (req, file, cb) => {
	if (
		file.mimetype ===
			'text/plain' ||
		file.mimetype === 
			'text/csv'
	) {
		// Accept a file
		cb(null, true);
	} else {
		// Reject a file
		cb(null, false);
	}
};

// Set upload limit to 100mb
const uploadTrain = multer({
	storage: storageTrain,
	limits: {
		fileSize: 1024 * 1024 * 1024,
	},
	fileFilter: fileFilter,
});

const uploadCompare = multer({
	storage: storageCompare,
	limits: {
		fileSize: 1024 * 1024 * 1024,
	},
	fileFilter: fileFilter,
});

router.post('/train', uploadTrain.array('themeFiles', 12), controller.upload_training_file);

router.post('/compare', uploadCompare.array('themeFiles', 12), controller.upload_file_to_match);

router.get('/train', controller.get_training_files);

router.get('/compare', controller.get_compare_files);

module.exports = router;
