/****************************
    File upload controller
 ****************************/

// Js libs
const mongoose = require('mongoose');
var fs = require('fs');

// Libs authored by team
const logger = require('~root/src/config').getLogger(__filename);
const config = require('~root/src/config');

// Declarations
const trainPath = config.uploadTrain;
const comparePath = config.uploadCompare;

module.exports.upload_training_file = async (req, res, next) => {
	try {
		res.status(200).json({result: "success"});
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(__filename);
		next(error);
	}
};

module.exports.upload_file_to_match = async (req, res, next) => {
	try {
		res.status(200).json({result: "success"});
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(__filename);
		next(error);
	}
};

module.exports.get_training_files = async (req, res, next) => {
	try {
		let response = [];
		const files = await get_files_in_dir(trainPath);
		for (let i = 0; i < files.length; i++) {
			const file = files[i];					
			let result = await get_file_data(trainPath + "/" +file);
			response.push({
				name: file,
				size: result.size
			})
		}
		res.status(200).json(response);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(__filename);
		next(error);
	}
};

module.exports.get_compare_files = async (req, res, next) => {
	try {
		let response = [];
		const files = await get_files_in_dir(comparePath);
		for (let i = 0; i < files.length; i++) {
			const file = files[i];					
			let result = await get_file_data(comparePath + "/" +file);
			response.push({
				name: file,
				size: result.size
			})
		}
		res.status(200).json(response);
	} catch (error) {
		if (!error.status) error.status = 500;
		logger.error(__filename);
		next(error);
	}
};


/***********************************
   Helper functions (not exported)
************************************/

// Get all files in given path
get_files_in_dir = (givenPath) => {
    return new Promise((resolve, reject) => {
        fs.readdir(givenPath, async (error, files) => {
            resolve(files);
        });
    });
};

// Get all files in given path
get_file_data = (givenPath) => {
    return new Promise((resolve, reject) => {
        fs.stat(givenPath, (error, stats) => {
			resolve(stats);
		});
    });
};