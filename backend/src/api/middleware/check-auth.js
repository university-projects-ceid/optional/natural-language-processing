const jwt = require('jsonwebtoken');
const config = require('~root/src/config');

/**
 * Middleware that validates a json web token stored as a cookie
 * in the request
 */
module.exports = (req, res, next) => {
	try {
		// const token = req.headers.authorization;
		// const decoded = jwt.verify(token, config.jwt_key);

		next();
	} catch (error) {
		return res.status(401).json({
			error,
			valid: false,
		});
	}
};
