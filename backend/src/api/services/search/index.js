const lemmaService = require('~services/lemma');
const Lemma = require("~models/Lemma");

/**
 * Get the search phrase from a url slug
 * @param {String} slug
 */
const getSearchPhrase = slug => {
	return slug.replace(/-/g, ' ');
};

/**
 * Prepare the search regex from the tokens given
 * @param {[String]} tokens
 */
const prepareRegex = tokens => {
	let regex = [];
	tokens.forEach(token => {
		console.log(token);
		if (token.split(' ').length === 1) {
			regex.push(new RegExp(`^${token}| ${token}|\-${token}`));
		} else {
			regex.push(new RegExp(token));
		}
	});
	console.log(regex);
	return regex;
};

/**
 * Find professional and category entities matching the query
 * @param {Object} query
 */
const getSearchResults = async query => {
	const results = await Lemma.find(query).sort({appearences: -1});
	return results;
};

module.exports = {
	getSearchPhrase,
	prepareRegex,
	getSearchResults,
};
