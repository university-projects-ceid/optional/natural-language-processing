const Characteristic = require('~models/Characteristic');

const getByQuery = async query => {
	const result = await Characteristic.find(query)
		.exec();
	return result;
};

const getOnlyVectors = async query => {
	const result = await Characteristic.find({idf:false, null:false},{tfIdfVector:1, features:1})
		.exec();
	return result;
};

const getNullVector = async () => {
	const result = await Characteristic.find({null:true})
    .lean();
	return result[0].vector;
};

const getIdfVector = async () => {
	const result = await Characteristic.find({idf:true})
    .lean();
	return result[0].vector;
};

const createCharacteristic = async characteristic => {
	const newCharacteristic = new Characteristic(characteristic);
	const result = await newCharacteristic.save();
	return result;
};

const deleteByQuery = async (query) => {
	const result = await Characteristic.deleteMany(query)
	.exec();
	return result;
}

module.exports = {
    getByQuery,
    getNullVector,
    getIdfVector,
    getOnlyVectors,
	createCharacteristic,
	deleteByQuery
};
