const Article = require('~models/Article');

const getByQuery = async query => {
	const result = await Article.find(query)
		.exec();
	return result;
};

const getById = async id => {
	const article = await getByQuery({ _id: id });
	if (article.length > 0) return article[0];
	return {};
};

const createArticle = async article => {
	const newArticle = new Article(article);
	const result = await newArticle.save();
	return result;
};

const deleteArticle = async id => {
	await Article.deleteOne({ _id: id }).exec();
};

module.exports = {
	getById,
	getByQuery,
	createArticle,
	deleteArticle
};
