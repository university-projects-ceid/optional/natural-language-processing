const Lemma = require('~models/Lemma');

const getByQuery = async query => {
	const result = await Lemma.find(query)
		.exec();
	return result[0];
};

const getById = async id => {
	const lemma = await getByQuery({ _id: id });
	if (lemma.length > 0) return lemma[0];
	return {};
};

const createLemma = async lemma => {
	const newLemma = new Lemma(lemma);
	const result = await newLemma.save();
	return result;
};

const deleteLemma = async id => {
	await Lemma.deleteOne({ _id: id }).exec();
};

const deleteByQuery = async (query) => {
	const result = await Lemma.deleteMany(query)
	.exec();
	return result;
}

module.exports = {
	getById,
	getByQuery,
	createLemma,
	deleteLemma,
	deleteByQuery
};
