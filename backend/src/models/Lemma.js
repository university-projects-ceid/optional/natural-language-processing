const mongoose = require('mongoose');

// Create the lemma model for mongoose
const lemmaSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
    name: String,
    appearences: Number,
    articles: {}
});

module.exports = mongoose.model('Lemma', lemmaSchema);
