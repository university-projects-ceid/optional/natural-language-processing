const mongoose = require('mongoose');

// Create the lemma model for mongoose
const characteristicSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
    features: [],
    vector: {},
    idf: {
        type: Boolean,
        default: false
    },
    null: {
        type: Boolean,
        default: false
    },
    tfIdfVector: {
        type: [],
        default: []
    }
});

module.exports = mongoose.model('Characteristic', characteristicSchema);
