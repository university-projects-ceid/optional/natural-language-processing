module.exports = {
	apps: [
		{
			name: 'ben',
			script: 'npm',
			args: 'start',
			env: {
				NODE_ENV: 'development',
			},
			env_production: {
				NODE_ENV: 'production',
			},
		},
	],
};
