import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ArticleComponent } from './pages/article/article.component';
import { SimilarityComponent } from './pages/similarity/similarity.component';


const routes: Routes = [
  { path: 'similarity', component: SimilarityComponent, data: { depth: 3 }},
  { path: '', component: HomeComponent, data: { depth: 1 }},
  { path: ':id', component: ArticleComponent, data: { depth: 2 }},
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
