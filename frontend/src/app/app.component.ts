import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { transitionAnimations } from './animations/transition.animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [transitionAnimations]
})
export class AppComponent {
  title = 'frontend';

  getDepth(outlet){
    return outlet.activatedRouteData['depth'];
  }

}
