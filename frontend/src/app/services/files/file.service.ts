import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private _http: HttpClient) { }


  uploadTrainingFiles(files) {
    let formData = new FormData();
    for (const file of files) {
      formData.append('themeFiles', file);
    }
    return this._http.post('http://localhost:5000/api/v1/file-upload/train', formData);
  }

  uploadComparisonFiles(files) {
    let formData = new FormData();
    for (const file of files) {
      formData.append('themeFiles', file);
    }
    return this._http.post('http://localhost:5000/api/v1/file-upload/compare', formData);
  }

  getTrainingFiles() {
    return this._http.get('http://localhost:5000/api/v1/file-upload/train');
  }
  
  getComparisonFiles() {
    return this._http.get('http://localhost:5000/api/v1/file-upload/compare');
  }

}
