import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CharacteristicService {

  constructor(private _http: HttpClient) { }

  trainCharacteristics() {
    return this._http.get('http://localhost:5000/api/v1/characteristics/train');
  }

  compareCharacteristics() {
    return this._http.get('http://localhost:5000/api/v1/characteristics/compare');
  }

}
