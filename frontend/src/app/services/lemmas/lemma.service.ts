import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LemmaService {

  constructor(private _http: HttpClient) { }

  // Get all the lemmas based on search terms
  searchTerms(payload: string){
    return this._http.post('http://localhost:5000/api/v1/search', {phrase: payload});
  }

}
