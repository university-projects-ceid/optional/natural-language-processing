import { TestBed } from '@angular/core/testing';

import { LemmaService } from './lemma.service';

describe('LemmaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LemmaService = TestBed.get(LemmaService);
    expect(service).toBeTruthy();
  });
});
