import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private _http: HttpClient) { }


  // Get all the articles in the db
  getArticles(){
    return this._http.get('http://localhost:5000/api/v1/articles/all');
  }

  getArticlesById(id) {
    return this._http.get('http://localhost:5000/api/v1/articles/'+id);
  }

}
