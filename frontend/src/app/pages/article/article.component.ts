import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from 'src/app/services/articles/article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  articleId: string;
  article: Object;
  loading: boolean = true;

  constructor(private _route: ActivatedRoute, private _article: ArticleService) {
    // Get the posts id from url
    this._route.paramMap.subscribe(params => {
      this.articleId = (params.get('id'));
      this._article.getArticlesById(this.articleId).subscribe(articleData => {
        this.loading = false;
        this.article = articleData;
      });
    });
  }

  ngOnInit() {
  }

}
