import { Component, OnInit } from '@angular/core';
import { ArticleService } from 'src/app/services/articles/article.service';
import { LemmaService } from 'src/app/services/lemmas/lemma.service';
import { listAnimationVertical,listAnimation } from 'src/app/animations/card.animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [listAnimationVertical, listAnimation] 
})
export class HomeComponent implements OnInit {
  articles: any;
  lemmas: any;
  timeout: any = null;
  found: any;

  constructor(private _articles: ArticleService, private _lemmas: LemmaService) {
    this.getAllArticles();
  }

  searchArticles(query: any) {
    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {      

      if (query != ""){
        this._lemmas.searchTerms(query.toLowerCase()).subscribe((articles: Array<Object>) => {
          this.articles = articles;
          this.found = articles.length;
        })

      }else{

        this.lemmas = [];
        this.found = 0;
        this.getAllArticles();

      }
    }, 300);
  }

  getAllArticles() {
    this._articles.getArticles().subscribe( articles => {
      this.articles = articles;
    });
  }

  ngOnInit() {
  }

}
