import { Component, OnInit } from '@angular/core';
import { FileService } from 'src/app/services/files/file.service';
import { listAnimationVertical, listAnimation } from 'src/app/animations/card.animations';
import { ViewEncapsulation } from '@angular/core';
import { CharacteristicService } from 'src/app/services/characteristics/characteristic.service';

@Component({
  selector: 'app-similarity',
  templateUrl: './similarity.component.html',
  styleUrls: ['./similarity.component.scss'],
  animations: [listAnimationVertical, listAnimation],
  encapsulation: ViewEncapsulation.None
})
export class SimilarityComponent implements OnInit {

  trainingFiles: any;
  comparisonFiles: any;
  trainingToUpload: any;
  comparisonToUpload: any;
  training: boolean;
  comparing: boolean;
  results: any;

  constructor(private _file: FileService, private _characteristic: CharacteristicService) { }


  processTrainingFile(event) {
    let files = event.target.files;
    this.trainingToUpload = files;
    console.log(files);
  }

  processComparisonFile(event) {
    let files = event.target.files;
    this.comparisonToUpload = files;
    console.log(files);
  }

  uploadTraining() {
    this._file.uploadTrainingFiles(this.trainingToUpload).subscribe(res =>{
        console.log("Training",res);
        this.trainingToUpload = null;
        this.getTrainingFiles();
    });
  }

  uploadCompare() {
    this._file.uploadComparisonFiles(this.comparisonToUpload).subscribe(res =>{
      console.log("Compare", res);
      this.comparisonToUpload = null;
      this.getComparisonFiles();
  });
  }

  getFileSize(size) {
    let fileSize = '0';
    let unit = "Bytes"
    if (size < 1000) {
      fileSize = size.toFixed(2);
      unit = "Bytes";
    } else if (size < 1000*1000) {
      fileSize = (size / 1000).toFixed(2);
      unit = "KB";
    } else if (size < 1000*1000*1000) {
      fileSize = (size / 1000 / 1000).toFixed(2);
      unit = "MB";
    } else {
      fileSize = (size / 1000 /1000 /1000).toFixed(2);
      unit = "GB";
    }
    return (fileSize + " " + unit);
  }

  trainWithCollection() {
    this.training = true;
    this._characteristic.trainCharacteristics().subscribe(res=>{
      this.training = false;
    });
  }

  compareWithCollection() {
    this.comparing = true;
    this._characteristic.compareCharacteristics().subscribe(res=>{
      this.comparing = false;
      this.results = res;
      console.log(res);
    });
  }

  getComparisonFiles() {
    this._file.getComparisonFiles().subscribe(comparisonFiles =>{
      this.comparisonFiles = comparisonFiles;
      console.log(comparisonFiles);
    });
  }

  getTrainingFiles() {
    this._file.getTrainingFiles().subscribe(trainingFiles =>{
      this.trainingFiles = trainingFiles;
      console.log(trainingFiles);
    });
  }

  ngOnInit() {
    this.getComparisonFiles();
    this.getTrainingFiles();
  }

}
