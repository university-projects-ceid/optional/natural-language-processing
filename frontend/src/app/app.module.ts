import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LottieAnimationViewModule } from 'ng-lottie';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HomeComponent } from './pages/home/home.component';
import { AppComponent } from './app.component';
import { ArticleComponent } from './pages/article/article.component';
import { SearchbarComponent } from './components/searchbar/searchbar.component';
import { SimilarityComponent } from './pages/similarity/similarity.component';
import { LoadingComponent } from './components/loading/loading.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ArticleComponent,
    SearchbarComponent,
    SimilarityComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LottieAnimationViewModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
