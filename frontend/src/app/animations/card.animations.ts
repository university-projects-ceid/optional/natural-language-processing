import { trigger, state, style, transition, animate, query, stagger, keyframes } from '@angular/animations';

export const listAnimation = [
    trigger('listAnimation',[
        transition('* <=> *', [
            query(':enter', style({opacity:0}), {optional: true}),
            query(':enter', stagger('100ms', [animate('400ms ease', keyframes([
                style({opacity: 0, transform: 'translateX(-35px)', offset:0}),
                style({opacity: 1, transform: 'translateX(0)', offset:1}),
            ]))]), {optional: true}),
        ])
    ])
]

export const listAnimationVertical = [
    trigger('listAnimationVertical',[
        transition('* <=> *', [
            query(':enter', style({opacity:0}), {optional: true}),
            query(':enter', stagger('100ms', [animate('400ms ease', keyframes([
                style({opacity: 0, transform: 'translateY(-35px)', offset:0}),
                style({opacity: 1, transform: 'translateY(0)', offset:1}),
            ]))]), {optional: true}),
        ])
    ])
]