import { trigger, state, style, query, group, transition, animate } from '@angular/animations';

export const transitionAnimations = [

    trigger('routeAnimation', [
        transition('1 => 2', [
            style({ height: '!' }),
            query(
                ':enter', style({ transform: 'translateX(110%)', opacity: 0})
            ),
            query (
                ':enter, :leave', style({ position: 'absolute', top: 0, left: 0, right: 0 })
            ),

            group([
                query(':leave', [animate('0.3s ease', style({ transform: 'translateX(-110%)', opacity: 0 }))]),
                query(':enter', [animate('0.3s ease' ,style({ transform: 'translateX(0%)', opacity: 1 }))])
            ])

        ]),

        transition('2 => 1', [
            style({ height: '!' }),
            query(
                ':enter', style({ transform: 'translateX(-110%)', opacity: 0})
            ),
            query(
                ':enter, :leave', style({ position: 'absolute', top: 0, left: 0, right: 0 })
            ),

            group([
                query(':leave', [animate('0.3s ease', style({ transform: 'translateX(110%)', opacity: 0 }))]),
                query(':enter', [animate('0.3s ease' ,style({ transform: 'translateX(0%)', opacity: 1 }))])
            ])

        ]),

    ])

]    
